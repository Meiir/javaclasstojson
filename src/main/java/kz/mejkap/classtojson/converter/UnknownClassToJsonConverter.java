package kz.mejkap.classtojson.converter;

public class UnknownClassToJsonConverter extends ClassConverter<Object> {


    public UnknownClassToJsonConverter(Class<Object> clazz) {
        this.clazz = clazz;
    }

    @Override
    public String toJson() {
        return "\"" + this.clazz.getName() + "\"";
    }

}
