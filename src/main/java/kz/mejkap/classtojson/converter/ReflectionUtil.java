package kz.mejkap.classtojson.converter;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public final class ReflectionUtil {

    /**
     * Получить все базовые интерфейсы. Например для HashMap получить Map
     *
     * @param clazz сотрудник
     * @return список базовых интерфейсов
     */
    public static Set<Class> getRootInterfaces(Class clazz) {
        Set result = new HashSet();
        collector(result, clazz);
        return result;
    }

    private static void collector(Set<Class> classes, Class clazz) {
        final Class[] interfaces = clazz.getInterfaces();
        if (interfaces.length == 0) {
            classes.add(clazz);
        } else {
            for (Class aClass : Arrays.asList(interfaces)) {
                collector(classes, aClass);
            }
        }
    }
}
