package kz.mejkap.classtojson.converter;

public class CharClassToJsonConverter extends ClassConverter<Character> {

    public CharClassToJsonConverter(Class<Character> clazz) {
        this.clazz = clazz;
    }

    @Override
    public String toJson() {
        return "\"char\"";
    }
}
