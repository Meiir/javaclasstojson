package kz.mejkap.classtojson.converter;

public class ShortClassToJsonConverter extends ClassConverter<Short> {

    public ShortClassToJsonConverter(Class<Short> clazz) {
        this.clazz = clazz;
    }

    @Override
    public String toJson() {
        return "\"short\"";
    }
}
