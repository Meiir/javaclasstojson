package kz.mejkap.classtojson.converter;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class CollectionClassToJsonConverter extends ClassConverter<List> {

    public CollectionClassToJsonConverter(Class<List> clazz) {
        this.clazz = clazz;
    }

    @Override
    public String toJson() {
        String start = "[";
        String end = ", \"...\"]";
        StringBuilder result = new StringBuilder();

        if (this.genericTypes.get(0) instanceof Class) {
            Class clazz = (Class) this.genericTypes.get(0);
            result.append(start);
            result.append(ConvertableClasses.getConverter(clazz, this.level).toJson());
            result.append(end);
        } else {
            final ParameterizedType parameterizedType = (ParameterizedType) this.genericTypes.get(0);
            final Class rawType = (Class) parameterizedType.getRawType();
            final List<Type> genericTypes = Arrays.stream(parameterizedType.getActualTypeArguments()).collect(Collectors.toList());
            result.append(ConvertableClasses.getConverter(rawType, this.level, genericTypes).toJson());
        }

        return result.toString();
    }
}
