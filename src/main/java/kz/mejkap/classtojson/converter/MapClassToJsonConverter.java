package kz.mejkap.classtojson.converter;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class MapClassToJsonConverter extends ClassConverter<Map> {

    public MapClassToJsonConverter(Class<Map> clazz) {
        this.clazz = clazz;
    }

    @Override
    public String toJson() {
        String start = "[{";
        String end = "}, \"...\"]";
        StringBuilder result = new StringBuilder();
        result.append(start);
        result.append(rowSeparator(false));

        result.append(ConvertableClasses.getConverter((Class) this.genericTypes.get(0), this.level + 1).toJson());
        result.append(": ");
        if (this.genericTypes.get(1) instanceof Class) {
            Class clazz = (Class) this.genericTypes.get(1);
            result.append(isCollectionStructure(clazz) ? "" : "[");
            result.append(ConvertableClasses.getConverter(clazz, this.level + 1).toJson());
            result.append(isCollectionStructure(clazz) ? "" : "]");
        } else {
            final ParameterizedType parameterizedType = (ParameterizedType) this.genericTypes.get(1);
            final Class rawType = (Class) parameterizedType.getRawType();
            final List<Type> genericTypes = Arrays.stream(parameterizedType.getActualTypeArguments()).collect(Collectors.toList());
            result.append(isCollectionStructure(rawType) ? "" : "[");
            result.append(ConvertableClasses.getConverter(rawType, this.level + 1, genericTypes).toJson());
            result.append(isCollectionStructure(rawType) ? "" : "]");
        }

        result.append(rowSeparator(true));
        result.append(end);
        return result.toString();
    }

    private boolean isCollectionStructure(Class clazz) {
        return clazz.equals(Map.class)
                || Arrays.asList(clazz.getInterfaces()).contains(Map.class)
                || ReflectionUtil.getRootInterfaces(clazz).contains(Map.class)
                || clazz.equals(Collection.class)
                || Arrays.asList(clazz.getInterfaces()).contains(Collection.class)
                || ReflectionUtil.getRootInterfaces(clazz).contains(Collection.class);
    }
}
