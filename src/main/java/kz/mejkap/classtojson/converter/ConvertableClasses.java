package kz.mejkap.classtojson.converter;

import kz.mejkap.classtojson.samples.convertable.*;

import java.lang.reflect.Type;
import java.util.*;

public enum ConvertableClasses {

    // Standard
    BOOLEAN(Set.of(Boolean.class, boolean.class), BooleanClassToJsonConverter.class),
    BYTE(Set.of(Byte.class, byte.class), ByteClassToJsonConverter.class),
    CHAR(Set.of(Character.class, char.class), CharClassToJsonConverter.class),
    DOUBLE(Set.of(Double.class, double.class), DoubleClassToJsonConverter.class),
    FLOAT(Set.of(Float.class, float.class), FloatClassToJsonConverter.class),
    INTEGER(Set.of(Integer.class, int.class), IntegerClassToJsonConverter.class),
    LONG(Set.of(Long.class, long.class), LongClassToJsonConverter.class),
    SHORT(Set.of(Short.class, short.class), ShortClassToJsonConverter.class),
    STRING(Set.of(String.class), StringClassToJsonConverter.class),
    COLLECTION(Set.of(List.class, Set.class), CollectionClassToJsonConverter.class),
    MAP(Set.of(Map.class), MapClassToJsonConverter.class),

    // User defined
    USER_DEFINED(Set.of(Person.class, PersonEntity.class, Statistics.class,
            PersonNamesMapping.class, PersonStatisticsMapping.class, SimpleMapExample.class),
            UserDefinedClassToJsonConverter.class);


    ConvertableClasses(Set<Class> convertableClasses, Class converterClazz) {
        this.convertableClasses = convertableClasses;
        this.converterClazz = converterClazz;
    }

    private final Set<Class> convertableClasses;
    private final Class converterClazz;

    public Set<Class> getConvertableClasses() {
        return convertableClasses;
    }


    public static ClassConverter getConverter(Class convertableClazz, int level) {
        return getConverter(convertableClazz, level, null);
    }

    /**
     * Получить конвертер из маппинга (настроек)
     *
     * @param convertableClazz класс, который может быть конвертирован в JSON
     * @param genericTypes     дополнительное поле для дженерик типов
     * @return Имплементацию ClassConverter. Если convertableClazz не входит в список конвертируемых, то возвращается UnknownClassToJsonConverter
     */
    public static ClassConverter getConverter(Class convertableClazz, int level, List<Type> genericTypes) {

        return Arrays.stream(values())
                .filter(value -> value.convertableClasses.contains(convertableClazz)
                        || !Collections.disjoint(value.convertableClasses, Arrays.asList(convertableClazz.getInterfaces()))
                        || !Collections.disjoint(value.convertableClasses, ReflectionUtil.getRootInterfaces(convertableClazz))
                )
                .findFirst()
                .map(value -> {
                    try {
                        ClassConverter converter = (ClassConverter) value.converterClazz
                                .getDeclaredConstructor(convertableClazz.getClass())
                                .newInstance(convertableClazz);
                        converter.setLevel(level);
                        if (genericTypes != null) {
                            converter.setGenericTypes(genericTypes);
                        }
                        return converter;
                    } catch (Exception e) {
                        return new UnknownClassToJsonConverter(convertableClazz);
                    }
                })
                .orElse(new UnknownClassToJsonConverter(convertableClazz));
    }
}
