package kz.mejkap.classtojson.converter;

public class FloatClassToJsonConverter extends ClassConverter<Float> {

    public FloatClassToJsonConverter(Class<Float> clazz) {
        this.clazz = clazz;
    }

    @Override
    public String toJson() {
        return "\"float\"";
    }
}
