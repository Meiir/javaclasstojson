package kz.mejkap.classtojson.converter;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class UserDefinedClassToJsonConverter extends ClassConverter<Object> {

    public UserDefinedClassToJsonConverter(Class<Object> clazz) {
        this.clazz = clazz;
    }

    @Override
    public String toJson() {
        String start = "{";
        String end = "}";
        StringBuilder result = new StringBuilder();
        result.append(start);
        result.append(rowSeparator(false));

        final List<Field> fields = this.fields();
        for (int i = 0; i < fields.size(); i++) {
            Field classField = fields.get(i);
            result.append("\"" + classField.getName() + "\": ");
            result.append(ConvertableClasses.getConverter(classField.getType(), this.level + 1, genericTypes(classField)).toJson());
            if (i < fields.size() - 1) {
                result.append(",");
                result.append(rowSeparator(false));
            } else {
                result.append(rowSeparator(true));
            }
        }

        result.append(end);
        return result.toString();
    }

    private List<Field> fields() {
        return Arrays.stream(this.clazz.getDeclaredFields())
                .collect(Collectors.toList());
    }

    private List<Type> genericTypes(Field field) {
        if (field.getGenericType() instanceof ParameterizedType) {
            return Arrays.stream(((ParameterizedType) field.getGenericType()).getActualTypeArguments())
                    .collect(Collectors.toList());
        } else {
            return List.of(field.getGenericType());
        }
    }
}
