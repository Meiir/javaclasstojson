package kz.mejkap.classtojson.converter;

public class ByteClassToJsonConverter extends ClassConverter<Byte> {

    public ByteClassToJsonConverter(Class<Byte> clazz) {
        this.clazz = clazz;
    }

    @Override
    public String toJson() {
        return "\"byte\"";
    }
}
