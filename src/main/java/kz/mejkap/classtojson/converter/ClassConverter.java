package kz.mejkap.classtojson.converter;

import java.lang.reflect.Type;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public abstract class ClassConverter<T> {

    protected int level;
    protected Class<T> clazz;
    protected List<Type> genericTypes;

    abstract public String toJson();

    protected String rowSeparator(boolean end) {
        return "\n" + IntStream.range(0, (end ? this.level : this.level + 1)).mapToObj(i -> "   ").collect(Collectors.joining());
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public void setGenericTypes(List<Type> genericTypes) {
        this.genericTypes = genericTypes;
    }
}
