package kz.mejkap.classtojson.converter;

public class StringClassToJsonConverter extends ClassConverter<String> {

    public StringClassToJsonConverter(Class<String> clazz) {
        this.clazz = clazz;
    }

    @Override
    public String toJson() {
        return "\"string\"";
    }
}
