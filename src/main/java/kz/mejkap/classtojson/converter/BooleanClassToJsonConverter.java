package kz.mejkap.classtojson.converter;

public class BooleanClassToJsonConverter extends ClassConverter<Boolean> {

    public BooleanClassToJsonConverter(Class<Boolean> clazz) {
        this.clazz = clazz;
    }

    @Override
    public String toJson() {
        return "\"boolean\"";
    }
}
