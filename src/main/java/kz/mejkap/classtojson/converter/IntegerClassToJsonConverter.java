package kz.mejkap.classtojson.converter;

public class IntegerClassToJsonConverter extends ClassConverter<Integer> {

    public IntegerClassToJsonConverter(Class<Integer> clazz) {
        this.clazz = clazz;
    }

    @Override
    public String toJson() {
        return "\"int\"";
    }
}
