package kz.mejkap.classtojson.converter;

public class DoubleClassToJsonConverter extends ClassConverter<Double> {

    public DoubleClassToJsonConverter(Class<Double> clazz) {
        this.clazz = clazz;
    }

    @Override
    public String toJson() {
        return "\"double\"";
    }
}
