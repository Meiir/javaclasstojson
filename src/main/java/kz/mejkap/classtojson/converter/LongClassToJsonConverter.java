package kz.mejkap.classtojson.converter;

public class LongClassToJsonConverter extends ClassConverter<Long> {

    public LongClassToJsonConverter(Class<Long> clazz) {
        this.clazz = clazz;
    }

    @Override
    public String toJson() {
        return "\"long\"";
    }
}
