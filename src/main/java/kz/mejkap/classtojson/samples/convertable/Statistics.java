package kz.mejkap.classtojson.samples.convertable;

public class Statistics {

    private char s;
    private int count;

    public char getS() {
        return s;
    }

    public void setS(char s) {
        this.s = s;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
