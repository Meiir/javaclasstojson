package kz.mejkap.classtojson.samples.convertable;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class SimpleMapExample {

    private Map<String, String> map;
    private HashMap<String, List<Boolean>> map1;
    private TreeMap<Integer, String> map2;

    public Map<String, String> getMap() {
        return map;
    }

    public void setMap(Map<String, String> map) {
        this.map = map;
    }

    public HashMap<String, List<Boolean>> getMap1() {
        return map1;
    }

    public void setMap1(HashMap<String, List<Boolean>> map1) {
        this.map1 = map1;
    }

    public TreeMap<Integer, String> getMap2() {
        return map2;
    }

    public void setMap2(TreeMap<Integer, String> map2) {
        this.map2 = map2;
    }
}
