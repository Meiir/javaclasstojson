package kz.mejkap.classtojson.samples.convertable;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class PersonNamesMapping {

    private List<String> names;
    private ArrayList<Person> persons;
    private Set<Statistics> statisticsSet;

    public List<String> getNames() {
        return names;
    }

    public void setNames(List<String> names) {
        this.names = names;
    }

    public ArrayList<Person> getPersons() {
        return persons;
    }

    public void setPersons(ArrayList<Person> persons) {
        this.persons = persons;
    }

    public Set<Statistics> getStatisticsSet() {
        return statisticsSet;
    }

    public void setStatisticsSet(Set<Statistics> statisticsSet) {
        this.statisticsSet = statisticsSet;
    }
}
