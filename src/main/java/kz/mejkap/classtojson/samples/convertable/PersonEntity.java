package kz.mejkap.classtojson.samples.convertable;

import kz.mejkap.classtojson.samples.notconvertable.Pair;

import java.io.File;

public class PersonEntity {

    private Long id;
    private Person meta;
    private Pair<Class, File> pair;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Person getMeta() {
        return meta;
    }

    public void setMeta(Person meta) {
        this.meta = meta;
    }

    public Pair<Class, File> getPair() {
        return pair;
    }

    public void setPair(Pair<Class, File> pair) {
        this.pair = pair;
    }
}
