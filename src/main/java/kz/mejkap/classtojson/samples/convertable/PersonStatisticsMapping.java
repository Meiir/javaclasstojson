package kz.mejkap.classtojson.samples.convertable;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class PersonStatisticsMapping {

    private double id;
    private Map<String, Set<Person>> personsStats;
    private Map<String, Map<Boolean, List<Statistics>>> activeStats;
    private List<Map<Boolean, List<Statistics>>> mapInList;

    public double getId() {
        return id;
    }

    public void setId(double id) {
        this.id = id;
    }

    public Map<String, Set<Person>> getPersonsStats() {
        return personsStats;
    }

    public void setPersonsStats(Map<String, Set<Person>> personsStats) {
        this.personsStats = personsStats;
    }

    public Map<String, Map<Boolean, List<Statistics>>> getActiveStats() {
        return activeStats;
    }

    public void setActiveStats(Map<String, Map<Boolean, List<Statistics>>> activeStats) {
        this.activeStats = activeStats;
    }

    public List<Map<Boolean, List<Statistics>>> getMapInList() {
        return mapInList;
    }

    public void setMapInList(List<Map<Boolean, List<Statistics>>> mapInList) {
        this.mapInList = mapInList;
    }
}
