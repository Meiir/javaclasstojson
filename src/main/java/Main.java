import kz.mejkap.classtojson.converter.ConvertableClasses;

import java.util.Comparator;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Main {

    private static List<Class> convertableClasses;

    public static void main(String... args) {

        /*for (Class aClass : ConvertableClasses.USER_DEFINED.getConvertableClasses()) {
            String json = ConvertableClasses.getConverter(aClass, 0).toJson();
            System.out.println(json);
            System.out.println();
        }*/

        convertableClasses = ConvertableClasses.USER_DEFINED.getConvertableClasses()
                .stream()
                .sorted(Comparator.comparing(Class::getSimpleName))
                .collect(Collectors.toList());

        while (true) {
            String choice = choose();
            if ("exit".equals(choice) || "quit".equals(choice)) {
                System.out.println("Bye!");
                System.exit(0);
            } else {
                try {
                    int num = Integer.parseInt(choice);
                    final Class aClass = convertableClasses.get(num - 1);
                    String json = ConvertableClasses.getConverter(aClass, 0).toJson();
                    System.out.println("JSON representation of class - " + aClass.getSimpleName());
                    System.out.println(json);
                    System.out.println();
                } catch (NumberFormatException | IndexOutOfBoundsException exp) {
                    System.out.println("Please, enter valid number between 1 and " + (convertableClasses.size()));
                }
            }
        }

    }

    private static String choose() {
        System.out.println("Classes available to convert: ");
        String answer = IntStream.range(0, convertableClasses.size())
                .mapToObj(i -> "" + (i + 1) + ". " + convertableClasses.get(i).getSimpleName())
                .collect(Collectors.joining(", \n"));
        System.out.println(answer);
        System.out.println("Please, choose any class name from list above entering number of: ");
        Scanner in = new Scanner(System.in);
        return in.nextLine();
    }
}
